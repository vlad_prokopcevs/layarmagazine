package com.layar.magazinedemo;

import java.text.SimpleDateFormat;
import java.util.Calendar;

import javax.security.auth.callback.Callback;

import android.content.ClipData;
import android.content.ClipboardManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;

import com.layar.sdk.LayarSDKClient;
import com.layar.sdk.LayarSDKFragment;
import com.layar.sdk.data.Action;
import com.layar.sdk.data.Layer;
import com.layar.sdk.data.Poi;
import com.layar.sdk.data.VisualSearchResult;

public class LayarSDKFragmentActivity extends FragmentActivity {
	public TextView text;
	public boolean curSett = true;

	private static final String TAG = Callback.class.getSimpleName();
	protected static final int MENU_ITEM_ITEM1 = 0;

	private LayarSDKFragment sdkFragment;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_layar_sdkfragment);

		text = (TextView) findViewById(R.id.text);
		Calendar cal = Calendar.getInstance();
		SimpleDateFormat sdf = new SimpleDateFormat("dd.MM.yyyy 'at' HH:mm:ss");
		text.setText("Fragment Created: " + sdf.format(cal.getTime()) + "\n\r");

		sdkFragment = (LayarSDKFragment) getSupportFragmentManager()
				.findFragmentById(R.id.sdk_fragment);
		sdkFragment.setSdkClient(sdkClient);
	}

	@Override
	protected void onStart() {
		super.onStart();
		sdkFragment.openScanMode();
	}

	@Override
	public void onBackPressed() {
		text = (TextView) findViewById(R.id.text);
		Log.d(TAG,
				"onBackPressed for the LayarSDKFragmentActivity has been called");
//		ClipboardManager clipboard = (ClipboardManager) getSystemService(CLIPBOARD_SERVICE);
//		clipboard.setPrimaryClip((ClipData) text.getText());
		recordToClipboard (text.getText());
		super.onBackPressed();
	}
	
	public void recordToClipboard(CharSequence s) {
		ClipData copyData = ClipData.newPlainText("Logs",s);			
		ClipboardManager clipboard = (ClipboardManager) getSystemService(CLIPBOARD_SERVICE);			
		clipboard.setPrimaryClip((ClipData) copyData);
	}

	private LayarSDKClient sdkClient = new LayarSDKClient() {

		public void appendTextView(String s) {
			Calendar cal = Calendar.getInstance();
			SimpleDateFormat sdf = new SimpleDateFormat("HH:mm:ss.SSS ");
			final String LogString = sdf.format(cal.getTime()) + s + "\n\r";
			text = (TextView) getSdkFragment().getActivity().findViewById(
					R.id.text);
			if ((text.getText().toString() == null)
					|| (text.getText().toString().isEmpty())) {
				// text.setText(s);
				sdkFragment.getActivity().runOnUiThread(new Runnable() {
					public void run() {
						text.setText(LogString);
					}
				});
			} else {
				// text.append(s);
				sdkFragment.getActivity().runOnUiThread(new Runnable() {
					public void run() {
						text.append(LogString);
					}
				});
				// text.setText(text.getText().toString()+s);
				//
			}
		}

		@Override
		public void onActionTriggered(Layer layer, Poi poi, Action action) {
			Log.d(TAG, "onActionTriggered: " + layer);
			Log.d(TAG, "onActionTriggered: " + poi);
			Log.d(TAG, "onActionTriggered: " + action);
			appendTextView("onActionTriggered, layer: " + layer + " poi: "
					+ poi + " action: " + action);
		}

		@Override
		public void onAugmentClicked(Layer layer, Poi poi) {
			Log.d(TAG, "onAugmentClicked: " + layer);
			Log.d(TAG, "onAugmentClicked: " + poi);
			appendTextView("onAugmentClicked, layer: " + layer + " poi: " + poi);
		}

		public void onAugmentFocused(Layer layer, Poi poi) {
			Log.d(TAG, "onAugmentFocused: " + layer);
			Log.d(TAG, "onAugmentFocused: " + poi);
			appendTextView("onAugmentFocused, layer: " + layer + " poi: " + poi);

		}

		@Override
		public void onAugmentShown(Layer layer, Poi poi) {
			Log.d(TAG, "onAugmentShown: " + layer);
			Log.d(TAG, "onAugmentShown: " + poi);
			appendTextView("onAugmentShown, layer: " + layer + " poi: " + poi);
		}

		@Override
		public boolean onBackPressed() {
			if (needsResetScanMode && !getSdkFragment().isInScanMode()) {
				needsResetScanMode = false;
				getSdkFragment().openScanMode();

				Log.d(TAG, "onBackPressed has been called");
				appendTextView("onBackPressed");
				//Copy logs to clipboard for later use
//				ClipData copyData = ClipData.newPlainText("Logs",text.getText());			
//				ClipboardManager clipboard = (ClipboardManager) getSystemService(CLIPBOARD_SERVICE);			
//				clipboard.setPrimaryClip((ClipData) copyData);
				recordToClipboard (text.getText());
				return true;
			}

			Log.d(TAG, "onBackPressed has been called");
			appendTextView("onBackPressed");
			//Copy logs to clipboard for later use
//			ClipData copyData = ClipData.newPlainText("Logs",text.getText());			
//			ClipboardManager clipboard = (ClipboardManager) getSystemService(CLIPBOARD_SERVICE);			
//			clipboard.setPrimaryClip((ClipData) copyData);
			recordToClipboard (text.getText());

			return false;
		}

		@Override
		public void onContentVisible(boolean contentVisible) {
			Log.d(TAG, "onContentVisible: " + contentVisible);
			appendTextView("onContentVisible, contentVisible: "
					+ String.valueOf(contentVisible));
		}

		@Override
		public void onCreateOptionsMenu(Menu menu) {
			super.onCreateOptionsMenu(menu);
			Log.d(TAG, "onCreateOptionsMenu: " + menu);
			appendTextView("onCreateOptionsMenu, menu: " + String.valueOf(menu));
			menu.add(Menu.NONE, MENU_ITEM_ITEM1, Menu.NONE, "Revert Settings");
		}

		@Override
		public void onLayerLaunched(Layer layer, String s) {
			super.onLayerLaunched(layer, s);

			getSdkFragment().setPopOutFitContent(curSett);
			getSdkFragment().setPopOut3DModeEnabled(curSett);
			getSdkFragment().setPopOut3DTiltEnabled(curSett);
			

			Calendar cal = Calendar.getInstance();
			SimpleDateFormat sdf = new SimpleDateFormat(
					"dd.MM.yyyy'at' HH:mm:ss");
			appendTextView("Layer launched: " + sdf.format(cal.getTime()));

			// FragmentActivity activity = getSdkFragment().getActivity();
			// FragmentManager fm = activity.getSupportFragmentManager();
			//
			// DialogFragment fragment = new DialogFragment();
			// fragment.show(activity.getSupportFragmentManager(), null);
		}

		// @Override
		// public void onActionTriggered(Layer layer, Poi poi, Action action) {
		// Toast.makeText(getSdkFragment().getActivity(),
		// "onActionTriggered:", Toast.LENGTH_SHORT).show();
		// }

		@Override
		public boolean onOptionsItemSelected(MenuItem item) {
			Log.d(TAG, "onCreateOptionsMenu: " + item);
			appendTextView("onCreateOptionsMenu, item: " + String.valueOf(item));
			switch (item.getItemId()) {
			case MENU_ITEM_ITEM1:
				curSett=!(curSett);
				return true;
				
			default:
				return false;
			}
		}

		public void onPopOutClosed(Layer layer,
				java.lang.String referenceImageId) {
			Log.d(TAG, "onPopOutClosed: " + layer);
			Log.d(TAG, "onPopOutClosed: " + referenceImageId);
			appendTextView("onPopOutClosed, layer: " + layer
					+ " referenceImageId: " + referenceImageId);
		}

		public void onPopOutTriggered(Layer layer,
				java.lang.String referenceImageId) {
			Log.d(TAG, "onPopOutTriggered: " + layer);
			Log.d(TAG, "onPopOutTriggered: " + referenceImageId);
			appendTextView("onPopOutTriggered, layer: " + layer
					+ " referenceImageId: " + referenceImageId);

		}

		@Override
		public void onQRCodeRecognized(java.lang.String qrCodeData) {
			Log.d(TAG, "onQRCodeRecognized: " + qrCodeData);
			appendTextView("onQRCodeRecognized, qrCodeData: " + qrCodeData);
		}

		public void onReferenceImageLost(Layer layer,
				java.lang.String referenceImageId) {
			Log.d(TAG, "onReferenceImageLost: " + layer);
			Log.d(TAG, "onReferenceImageLost: " + referenceImageId);
			appendTextView("onReferenceImageLost, layer: " + layer
					+ " referenceImageId: " + referenceImageId);
			// getSdkFragment().triggerPopOut(referenceImageId);

		}

		@Override
		public void onReferenceImageTracked(Layer layer,
				java.lang.String referenceImageId) {
			// getSdkFragment().openScreenshotMode();
			Log.d(TAG, "onReferenceImageTracked: " + layer);
			Log.d(TAG, "onReferenceImageTracked: " + referenceImageId);
			appendTextView("onReferenceImageTracked, layer: " + layer
					+ " image: " + referenceImageId);
			getSdkFragment().closePopOut();

		}

		@Override
		public void onScreenshotModeClosed() {
			Log.d(TAG, "onScreenshotModeClosed");
			Calendar cal = Calendar.getInstance();
			SimpleDateFormat sdf = new SimpleDateFormat(
					"dd.MM.yyyy'at' HH:mm:ss");
			appendTextView("onScreenshotModeClosed: "
					+ sdf.format(cal.getTime()));
		}

		@Override
		public void onScreenshotModeOpened() {
			Log.d(TAG, "onScreenshotModeOpened");
			Calendar cal = Calendar.getInstance();
			SimpleDateFormat sdf = new SimpleDateFormat(
					"dd.MM.yyyy'at' HH:mm:ss");
			appendTextView("onScreenshotModeOpened: "
					+ sdf.format(cal.getTime()));
		}

		@Override
		public void onVideoPlaybackStarted(Layer layer, Poi poi) {
			Log.d(TAG, "onVideoPlaybackStarted: " + layer);
			Log.d(TAG, "onVideoPlaybackStarted: " + poi);
			appendTextView("onVideoPlaybackStarted, layer: " + layer + " poi: "
					+ poi);
		}

		@Override
		public void onVisualSearchResult(int responseCode,
				java.lang.String responseMessage, VisualSearchResult[] results) {
			Log.d(TAG, "onVisualSearchResult: " + responseCode);
			Log.d(TAG, "onVisualSearchResult: " + responseMessage);
			Log.d(TAG, "onVisualSearchResult: " + results);
			appendTextView("onVisualSearchResult, responseCode: "
					+ responseCode + " responseMessage: " + responseMessage
					+ " results: " + results);
		}

		public boolean shoudPerformVisualSearch(java.io.File queryImage) {
			Log.d(TAG, "shouldPerformVisualsearch: " + queryImage);
			appendTextView("shouldPerformVisualsearch, queryImage: "
					+ queryImage);
			return true;
		}

		public boolean shouldShowNoResultsFound(java.io.File queryImage) {
			Log.d(TAG, "shouldShowNoResultsFound: " + queryImage);
			appendTextView("shouldShowNoResultsFound, queryImage: "
					+ queryImage);
			return true;
		}

		private boolean needsResetScanMode = false;

		@Override
		public boolean shouldTriggerAction(Layer layer, Poi poi, Action action) {

			Uri uri = Uri.parse(action.getUri());
			if ("layar".equals(uri.getScheme())) {
				needsResetScanMode = true;

				String layerName = uri.getAuthority();
				getSdkFragment().openLayer(layerName);
				Log.d(TAG, "shouldTriggerAction: " + layer);
				Log.d(TAG, "shouldTriggerAction: " + poi);
				Log.d(TAG, "shouldTriggerAction: " + action);
				appendTextView("shouldTriggerAction, layer: " + layer
						+ " poi: " + poi + " action: " + action);
				return false;
//				return true;
			}

			return true;
		}

	};
}
