package com.layar.magazinedemo;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;

import com.layar.sdk.LayarSDK;
import com.layar.sdk.LayarSDKClient;



public class MainActivity extends Activity implements OnClickListener {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		
		findViewById(R.id.buttonscan).setOnClickListener(this);
		findViewById(R.id.buttonplay).setOnClickListener(this);
		findViewById(R.id.buttonpartner).setOnClickListener(this);
		findViewById(R.id.SDKinfo).setOnClickListener(this);
		findViewById(R.id.SDKFragment).setOnClickListener(this);
		
//		LayarSDK.initialize(this, "jpUBQeWtwObCSyfo", "CVcASxNgEwfvmKqaJjkTPiUpudZFLseB"); //old credentials		
//		LayarSDK.initialize(this, "HotvcfSAZaFhlxnd", "FArgGTWbmBdZaohYEkHLjlzuyOQwCRsN"); //Yas Marine
		LayarSDK.initialize(this, "DZqzMYSAOKjliTPe", "UrSzTxnJRblCedhuPgmpGVcjMAvQqDsE"); //Layar magazine and OnePlus
	}
	
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.buttonscan:
			LayarSDK.startLayarActivity(this, new DemoSdkClient());
			break;
		case R.id.buttonplay:
			Intent intent = new Intent(Intent.ACTION_VIEW);
			intent.setData(Uri.parse("market://details?id=com.layar&hl=en"));
			startActivity(intent);
			break;
		case R.id.SDKinfo:
			startActivity(new Intent(
					Intent.ACTION_VIEW,
					Uri.parse("https://www.layar.com/products/custom-solutions/sdk/")));
			break;
		case R.id.buttonpartner:
			LayarSDK.startLayarActivity(this, new DemoSdkClient(), "rsdevelopersharing");
			break;
		case R.id.SDKFragment:
			Intent intent2 = new Intent(this, LayarSDKFragmentActivity.class);
			startActivity(intent2);
			break;
		}
		
	};
	
	

}
