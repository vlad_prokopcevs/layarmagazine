package com.layar.magazinedemo;

import java.io.File;

import javax.security.auth.callback.Callback;

import android.net.Uri;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;

import com.layar.sdk.LayarSDKClient;
import com.layar.sdk.LayarSDKFragment;
import com.layar.sdk.data.Action;
import com.layar.sdk.data.Layer;
import com.layar.sdk.data.Poi;
import com.layar.sdk.data.VisualSearchResult;

public class DemoSdkClient extends LayarSDKClient {
	
	private static final String TAG = Callback.class.getSimpleName();
	private static final int MENU_ITEM_ITEM1 = 0;
	public boolean curSett = false;

	@Override	
	public void onLayerLaunched(Layer layer, java.lang.String referenceImageId){
		Log.d(TAG, "onLayerLaunched: " + layer);
		Log.d(TAG, "onLayerLaunched: " + referenceImageId);
		
		//getSdkFragment().openScreenshotMode();
		getSdkFragment().setPopOut3DModeEnabled(curSett);
		//getSdkFragment().setDefaultPopOutUIEnabled(false);
		
		getSdkFragment().setPopOut3DTiltEnabled(curSett);
		
		getSdkFragment().setPopOutShowPreviewImage(curSett);
		getSdkFragment().setPopOutTrackingEnabled(curSett);
	}
	
	@Override	
	public void onReferenceImageTracked(Layer layer, java.lang.String referenceImageId){
	//	getSdkFragment().openScreenshotMode();
		Log.d(TAG, "onReferenceImageTracked: " + layer);
		Log.d(TAG, "onReferenceImageTracked: " + referenceImageId);	
	    getSdkFragment().closePopOut() ;
	
	}
	
	@Override
	public void onAugmentShown(Layer layer, Poi poi){  
		Log.d(TAG, "onAugmentShown: " + layer);
		Log.d(TAG, "onAugmentShown: " + poi);
	}

	
	@Override
    public void onActionTriggered(Layer layer, Poi poi, Action action){
		Log.d(TAG, "onActionTriggered: " + layer);
		Log.d(TAG, "onActionTriggered: " + poi);	
		Log.d(TAG, "onActionTriggered: " + action);
    }
	@Override    
    public void onAugmentClicked(Layer layer, Poi poi){
		Log.d(TAG, "onAugmentClicked: " + layer);
		Log.d(TAG, "onAugmentClicked: " + poi);
	}

    public void onPopOutTriggered(Layer layer, java.lang.String referenceImageId){
    	Log.d(TAG, "onPopOutTriggered: " + layer);
		Log.d(TAG, "onPopOutTriggered: " + referenceImageId);	
    	
    }
    
    public void onPopOutClosed(Layer layer, java.lang.String referenceImageId){
    	
    	Log.d(TAG, "onPopOutClosed: " + layer);
		Log.d(TAG, "onPopOutClosed: " + referenceImageId);	
    	
    }
    
    public void onReferenceImageLost(Layer layer, java.lang.String referenceImageId){
    
    	Log.d(TAG, "onReferenceImageLost: " + layer);
		Log.d(TAG, "onReferenceImageLost: " + referenceImageId);
    	
	//	getSdkFragment().triggerPopOut(referenceImageId);
    	
    }
    
	
	@Override   
    public void onQRCodeRecognized(java.lang.String qrCodeData){
		Log.d(TAG, "onQRCodeRecognized: " + qrCodeData);
    }
	@Override   
    public void onVideoPlaybackStarted(Layer layer, Poi poi){
		Log.d(TAG, "onVideoPlaybackStarted: " + layer);
		Log.d(TAG, "onVideoPlaybackStarted: " + poi);
    }
	@Override   
    public void onVisualSearchResult(int responseCode, java.lang.String responseMessage, VisualSearchResult[] results){
		Log.d(TAG, "onVisualSearchResult: " + responseCode);
		Log.d(TAG, "onVisualSearchResult: " + responseMessage);	
		Log.d(TAG, "onVisualSearchResult: " + results);
    	}
  

	public boolean shoudPerformVisualSearch(java.io.File queryImage){
    	Log.d(TAG, "shouldPerformVisualsearch: " + queryImage);
    	return true;	
    }
    
	
	
    public boolean shouldShowNoResultsFound(java.io.File queryImage){
    	Log.d(TAG, "shouldShowNoResultsFound: " + queryImage);
    	return true;	
    }

    public void onAugmentFocused(Layer layer, Poi poi){
    	Log.d(TAG, "onAugmentFocused: " + layer);
		Log.d(TAG, "onAugmentFocused: " + poi);	
    	
    	
    }
	
	
	private boolean needsResetScanMode = false;
	
	@Override
	public boolean shouldTriggerAction(Layer layer, Poi poi, Action action) {
		
		Uri uri = Uri.parse(action.getUri());
		if ("layar".equals(uri.getScheme())) {
			needsResetScanMode = true;
			
			String layerName = uri.getAuthority();
			getSdkFragment().openLayer(layerName);
			Log.d(TAG, "shouldTriggerAction: " + layer);
			Log.d(TAG, "shouldTriggerAction: " + poi);	
			Log.d(TAG, "shouldTriggerAction: " + action);
			return false;
//			return true;
		}
		
		return true;
	}
	
	@Override
	public boolean onBackPressed() {
		if (needsResetScanMode && !getSdkFragment().isInScanMode()) {
			needsResetScanMode = false;
			
			getSdkFragment().openScanMode();
			return true;
		}
		Log.d(TAG, "onBackPressed has been called");
		return false;
	}
	
	@Override
	public void onCreateOptionsMenu(Menu menu) {
		super.onCreateOptionsMenu(menu);
		Log.d(TAG, "onCreateOptionsMenu: " + menu);
		//appendTextView("onCreateOptionsMenu, menu: " + String.valueOf(menu));
		menu.add(Menu.NONE, MENU_ITEM_ITEM1, Menu.NONE, "Current: " + curSett);
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		Log.d(TAG, "onCreateOptionsMenu: " + item);
		//appendTextView("onCreateOptionsMenu, item: " + String.valueOf(item));
		switch (item.getItemId()) {
	    case MENU_ITEM_ITEM1:
	    	curSett=!(curSett);
	    	item.setTitle("Current: " + curSett);
	        return true;

	    default:
	    	return false;
		}
	}
	
}
